#include <vector>
#include <iostream>
#include <bitset>
#include "BitArrayRepres.h"

int main() {
	//std::cout << std::bitset<8 * sizeof(uint16_t)>(65534) << std::endl;

	//BitArrayRepres bitArr = BitArrayRepres(10, 12);
	//auto a = bitArr.Append(10);

	uint16_t a = 4095;
	std::cout << std::bitset<8 * sizeof(uint16_t)>(a) << std::endl;
	a = a >> 8;
	std::cout << std::bitset<8 * sizeof(uint16_t)>(a) << std::endl;
	uint8_t b = a;
	std::cout << std::bitset<8 * sizeof(uint8_t)>(b) << std::endl;

	return 0;
}