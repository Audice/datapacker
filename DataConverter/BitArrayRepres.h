#pragma once
#include <cstdint>
#include <vector>

class BitArrayRepres
{
private:
	/// <summary>
	/// ��������� ��� �������� �����
	/// </summary>
	uint8_t* BitContainer;
	/// <summary>
	/// ������ ����������. ������������� ������
	/// </summary>
	uint32_t ContainerSize;
	/// <summary>
	/// ������������ ���������� ��������� � ����������
	/// </summary>
	uint64_t NumElem;
	/// <summary>
	/// ���������� ��� �� �������. ��� ������������� uint16_t
	/// </summary>
	uint8_t BitsPerElement;
	/// <summary>
	/// ������������ �������� 
	/// </summary>
	uint16_t MaxValue;
	/// <summary>
	/// ����������� ��������
	/// </summary>
	uint16_t MinValue;

	uint64_t MaxIndexBitArr;
	/// <summary>
	/// ������ ���������� ���������
	/// </summary>
	size_t CurIndexBitArr;


public:
	/// <summary>
	/// ����������� ������ ����������� �������� ������
	/// </summary>
	/// <param name="numElem">���������� ���������� ��������� � ����������</param>
	/// <param name="bitsPerElement">����� ��� � ���������� ��������</param>
	BitArrayRepres(uint64_t numElem, uint8_t bitsPerElement) {
		this->NumElem = numElem;
		this->BitsPerElement = bitsPerElement;
		if ((numElem * bitsPerElement) % (8 * sizeof(uint8_t)) == 0) {
			this->ContainerSize = (numElem * bitsPerElement) / (8 * sizeof(uint8_t));
		}
		else {
			this->ContainerSize = (numElem * bitsPerElement) / (8 * sizeof(uint8_t)) + 1;
		}
		this->BitContainer = new uint8_t[this->ContainerSize];
		CurIndexBitArr = 0;
	}

	uint8_t* Append(uint16_t newVal) {
		uint8_t* result = nullptr;
		//�������� ������ � �������������� ������
		if (this->NumElem <= CurIndexBitArr) {
			result = new uint8_t[this->ContainerSize];
			for (size_t i = 0; i < this->ContainerSize; i++) {
				result[i] = BitContainer[i];
			}
			//�������� ������ � �������� ���������� � ������ 
			CurIndexBitArr = 0;
		}
		else {
			size_t conteinIndex = (this->CurIndexBitArr * this->BitsPerElement) / (8 * sizeof(uint8_t));
			uint8_t startBit = (this->CurIndexBitArr * this->BitsPerElement) % (8 * sizeof(uint8_t));
			
			uint8_t partsArrSize = startBit == 0 ? 0 : 1;

			uint8_t tmp = (this->BitsPerElement - startBit) / (8 * sizeof(uint8_t));

			if (tmp == 0) {
				if (tmp % (8 * sizeof(uint8_t)) > 0)
					partsArrSize++;
			}
			else {
				partsArrSize += tmp;
				if (tmp % (8 * sizeof(uint8_t)) > 0)
					partsArrSize++;
			}

			uint8_t* newValParts = new uint8_t[partsArrSize];
			//��������� ������
			newValParts[0] = newVal << startBit;


		}
		return result;
	}

	void PrintConteiner() {
		std::vector<uint16_t> outVec;


	}





};

